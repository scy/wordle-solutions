# Solution Data

This directory contains solution data, one file per month.

Each file is a YAML dictionary with the key being the date, and the value another dictionary with each of the sites I’ve attempted on that day, e.g. `DE` (for 6mal5), `EN` (for the original Wordle) and `Heardle`.

## Format for `EN` & `DE`

These are simple space-delimited strings containing 1 to 7 words with 5 letters each.
(I found it too verbose to write down real arrays/lists.)

The last word of the list is always the solution.

* If there is just one word in the list, I solved it on the first attempt.
* Two words in the list mean that I solved it on the second attempt.
* Seven words mean that I didn’t manage to solve it after six attempts. The seventh word is the solution.

## Format for `Heardle`

This is a list of attempts.
Each attempt is a tuple of artist, title and optionally release year.

Again, the last item in the list is the solution.
However, there are some special strings that can be used instead of an attempt tuple:

* `skip` means that I’ve skipped this attempt, i.e. not entered any guess at all, in order to get a longer sample.
* `3 skip` (or `3 skips`, or `skip 3`) means 3 skips in a row.
* `fail` is short for “as many skips as required to fail the Heardle” and is for example used if I skip all of the attempts without guessing anything.
