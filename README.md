# My Wordle (& Co.) solutions

In this repository I’m tracking my solutions to

* [Wordle](https://www.nytimes.com/games/wordle/index.html)
* [6mal5](https://6mal5.com/) (a German version)
* [Heardle](https://www.heardle.app/) (for music)

Find the solutions in the [YAML](https://en.wikipedia.org/wiki/YAML) files in [`solutions`](solutions).
🚨 **Spoiler Warning:** These may very much include today’s solutions, too.

## Visualization & Statistics

My plan is to write some code that will render HTML from these results in order to be displayed on a website.
However, I haven’t done that yet.

## License

The solution data is licensed under [CC0 1.0 Universal](solutions/license.txt).
